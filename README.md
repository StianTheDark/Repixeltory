A repository for pixel art licensed under CC BY.

## Contributing

Just contribute on! If it looks fine, I'll accept it! (The barrier for entry isn't
very high) However, on certain groups of art, we suggest using a template. This includes;

Flags. (Norway for cross, Russia for stripe and France for horizontal stripes.)